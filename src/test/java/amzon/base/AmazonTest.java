package amzon.base;

import org.testng.annotations.Test;

import amazon.actions.LetsShopPageActions;

public class AmazonTest extends BaseTest {

	@Test
	public void verifyItemListedInCart() {
		LetsShopPageActions lets_shop_page_actions = new LetsShopPageActions();
		lets_shop_page_actions.clickOnRegisterHereLink();
		lets_shop_page_actions.enterFirstName("Yashodhara");
		lets_shop_page_actions.enterLastName("Bangera");
		lets_shop_page_actions.enterEmail("dummy1183@mail.nil");
		lets_shop_page_actions.enterPhoneNumber("4526359865");
		lets_shop_page_actions.selectOccupation("Doctor");
		lets_shop_page_actions.clickOnGenderRadioBtn("Male");
		lets_shop_page_actions.enterPassword("Yashu@88616");
		lets_shop_page_actions.enterConfirmPassword("Yashu@88616");
		lets_shop_page_actions.clickOnAgeCheckbox();
		lets_shop_page_actions.clickOnLoginOrRegisterBtn();
		lets_shop_page_actions.clickOnLoginBtn();
		lets_shop_page_actions.enterEmail("dummy1183@mail.nil");
		lets_shop_page_actions.enterPassword("Yashu@88616");
		lets_shop_page_actions.clickOnLoginOrRegisterBtn();
		lets_shop_page_actions.clickOnAddToCartBtn();
		lets_shop_page_actions.verifyToastMsg("Product Added To Cart");

	}

}
