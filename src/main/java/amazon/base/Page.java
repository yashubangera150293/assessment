package amazon.base;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Page {

	public static WebDriver driver;
//	public static WebDriverWait wait;

	public void initConfiguration() {

		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
//			options.addArguments("--headless");
		driver = new ChromeDriver(options);

		driver.manage().window().maximize();
		driver.manage().window().setSize(new Dimension(1366, 768));
		driver.get("https://rahulshettyacademy.com/client");

	}

	public void quitBrowser() {
		driver.quit();
	}

	// Click action

	public void click(WebElement element) {
//		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	// Type action
	public void type(WebElement element, String value) {
//		wait.until(ExpectedConditions.visibilityOf(element));
		element.clear();
		element.sendKeys(value);

	}

	// Select action
	public void select(WebElement element, String value) {
//		wait.until(ExpectedConditions.textToBePresentInElement(element, value));
		Select select = new Select(element);

		if (select.isMultiple()) {
			select.deselectAll();
		}

		select.selectByVisibleText(value);
	}

	// Verification
	public void verifyEquals(String expected, String actual) {
		try {
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		} catch (Throwable t) {
			assert false : "Expected is " + expected + " but we found " + actual;
			t.printStackTrace();
		}
	}

	// Click checkbox
	public void clickCheckbox(WebElement checkbox) {
		if (!checkbox.isSelected()) {
			click(checkbox);
		}
	}

}
