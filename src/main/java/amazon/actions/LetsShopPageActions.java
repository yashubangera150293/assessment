package amazon.actions;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import amazon.base.Page;
import amazon.locators.LetsShopPageLocators;

public class LetsShopPageActions extends Page {

	LetsShopPageLocators lets_shop_page_locators;
	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

	public LetsShopPageActions() {
		this.lets_shop_page_locators = new LetsShopPageLocators();
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 5);
		PageFactory.initElements(factory, this.lets_shop_page_locators);
	}

	/*
	 * button
	 */

	public void clickOnLoginOrRegisterBtn() {
		click(lets_shop_page_locators.login_register_btn);
	}

	public void clickOnLoginBtn() {
		click(lets_shop_page_locators.login_btn);
	}

	public void clickOnAddToCartBtn() {
		click(lets_shop_page_locators.add_to_cart_btn);
	}

	/*
	 * Checkbox
	 */

	public void clickOnAgeCheckbox() {
		clickCheckbox(lets_shop_page_locators.age_checkbox);
	}

	/*
	 * Link
	 */

	public void clickOnRegisterHereLink() {
		click(lets_shop_page_locators.register_here_link);
	}

	/*
	 * Textbox
	 */

	public void enterFirstName(String first_name) {
		type(lets_shop_page_locators.firstname_textbox, first_name);
	}

	public void enterLastName(String last_name) {
		type(lets_shop_page_locators.last_name_textbox, last_name);
	}

	public void enterEmail(String email) {
		type(lets_shop_page_locators.email_textbox, email);
	}

	public void enterPhoneNumber(String phone_number) {
		type(lets_shop_page_locators.phone_number_textbox, phone_number);
	}

	public void enterPassword(String password) {
		type(lets_shop_page_locators.password_textbox, password);
	}

	public void enterConfirmPassword(String confirmed_password) {
		type(lets_shop_page_locators.confirm_password_textbox, confirmed_password);
	}

	/*
	 * dropdown
	 */

	public void selectOccupation(String occupation) {
		select(lets_shop_page_locators.occupation_dropdown, occupation);
	}

	/*
	 * Radio button
	 */

	public void clickOnGenderRadioBtn(String gender) {
		wait.until(ExpectedConditions.visibilityOfAllElements(lets_shop_page_locators.gendar_radio_btn));
		for (WebElement gender_radio_btn : lets_shop_page_locators.gendar_radio_btn) {
			if (gender_radio_btn.getAttribute("value").equals(gender)) {
				click(gender_radio_btn);
			}
		}
	}

	/*
	 * Toast mesage
	 */

	public void verifyToastMsg(String expected_toast_msg) {
//		try {
//			Thread.sleep(500);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		wait.until(ExpectedConditions.visibilityOf(lets_shop_page_locators.toast_msg));
		System.out.println(lets_shop_page_locators.toast_msg.getText());
		verifyEquals(expected_toast_msg, lets_shop_page_locators.toast_msg.getText());
	}
}
