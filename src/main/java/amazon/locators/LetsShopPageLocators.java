package amazon.locators;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LetsShopPageLocators {

	/*
	 * button
	 */

	@FindBy(id = "login")
	public WebElement login_register_btn;

	@FindBy(xpath = "//button[@class='btn btn-primary']")
	public WebElement login_btn;

	@FindBy(xpath = "//b[text()='ADIDAS ORIGINAL']//parent::h5/following-sibling::button[2]")
	public WebElement add_to_cart_btn;

	/*
	 * Checkbox
	 */

	@FindBy(xpath = "//input[@class='ng-untouched ng-pristine ng-invalid']")
	public WebElement age_checkbox;

	/*
	 * Link
	 */

	@FindBy(xpath = "//p[@class='login-wrapper-footer-text']")
	public WebElement register_here_link;

	/*
	 * Textbox
	 */

	@FindBy(id = "firstName")
	public WebElement firstname_textbox;

	@FindBy(id = "lastName")
	public WebElement last_name_textbox;

	@FindBy(id = "userEmail")
	public WebElement email_textbox;

	@FindBy(id = "userMobile")
	public WebElement phone_number_textbox;

	@FindBy(id = "userPassword")
	public WebElement password_textbox;

	@FindBy(id = "confirmPassword")
	public WebElement confirm_password_textbox;

	/*
	 * dropdown
	 */

	@FindBy(xpath = "//select[contains(@class,'custom-select')]")
	public WebElement occupation_dropdown;

	/*
	 * Radio button
	 */

	@FindBy(xpath = "//input[@class='mt-3 ng-untouched ng-pristine ng-valid']")
	public List<WebElement> gendar_radio_btn;

	/*
	 * Toast mesage
	 */

	@FindBy(xpath = "//div[contains(@class,'toast-message')]") //// div[text()=' Product Added To Cart ']
	public WebElement toast_msg;

}
