package amzon.base;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import amazon.base.Page;

public class BaseTest extends Page {

	@BeforeTest
	public void openTheAmazonWebsite() {
		initConfiguration();
	}

	@AfterTest
	public void quitTheBrowser() {
		quitBrowser();
	}

}
